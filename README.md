# docker-nzbhydra2 Docker image

[![pipeline status](https://gitlab.com/homesrvr/docker-nzbhydra2/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-nzbhydra2/commits/main) 
[![Sabnzbd Release](https://gitlab.com/homesrvr/docker-nzbhydra2/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-nzbhydra2/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker link](https://gitlab.com/homesrvr/docker-nzbhydra2/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/nzbhydra2)

This is a alpine-based dockerized build of [nzbhydra2](https://github.com/theotherp/nzbhydra2 "nzbhydra2 Github Project Homepage").
Part of a collection of docker images, designed to run on my low-end x86 based QNAP NAS server. My aim is to keep this a lightweight image.

## Example usage

**docker run example**
```yaml
docker run -d \
  -v [/configdir]:/config \
  -v [/completedir]:/complete \
  -v [/incompletedir]:/incomplete \
  -p 5076:5076 \
  --restart=unless-stopped culater/nzbhydra2
```

**docker-compose example:**
```yaml
---
version: "2.1"
services:
  nzbhydra2:
    image: culater/nzbhydra2:latest
    container_name: nzbhydra2
    hostname: nzbhydra2
    environment:
    - PUID=1001
    - PGID=100
    - TZ=Europe/Berlin
    volumes:
    - /config:/config
    - /downloads:/downloads
    ports:
    - 5076:5076
    restart: always
```

## Reporting problems
Please report any issues to the [Gitlab issue tracker](https://gitlab.com/homesrvr/docker-nzbhydra2/-/issues)

## Authors and acknowledgment
More information about nzbhydra2 can be found here:
[nzbhydra2](https://github.com/theotherp/nzbhydra2 "nzbhydra2 Github Project Homepage") 

## Project status
The docker image auto-updates weekly and should catch up to the latest nzbhydra2 release. 

