FROM alpine:3.16

LABEL   maintainer = "cusoon" \
        org.label-schema.name = "NZBHydra2" \
        org.label-schema.url="https://github.com/theotherp/nzbhydra2" 

# Defaults
ENV MAXMEM="256M"
ENV NZBHYDRA2_VER="empty"

SHELL ["/bin/ash", "-o", "pipefail", "-c"]
WORKDIR /app/nzbhydra2/bin
RUN \
    apk add --no-cache jq unzip openjdk16-jre nss tzdata tini curl && \
    NZBHYDRA2_VERSION=$(curl -sX GET 'https://api.github.com/repos/theotherp/nzbhydra2/releases/latest' | jq -r .tag_name) && \
    NZBHYDRA2_VER=${NZBHYDRA2_VERSION#v} && \
    export NZBHYDRA2_VER && \
    echo "https://github.com/theotherp/nzbhydra2/releases/download/v${NZBHYDRA2_VER}/nzbhydra2-${NZBHYDRA2_VER}-linux.zip" && \
    curl -s -L -o /tmp/nzbhydra2.zip "https://github.com/theotherp/nzbhydra2/releases/download/v${NZBHYDRA2_VER}/nzbhydra2-${NZBHYDRA2_VER}-linux.zip"  && \
    unzip /tmp/nzbhydra2.zip -d /tmp/nzbhydra2 && \
    cp "/tmp/nzbhydra2/lib/core-${NZBHYDRA2_VER}-exec.jar" "/tmp/nzbhydra2/LICENSE" . && \
    echo "NZBHYDRA2_VER=$NZBHYDRA2_VER" > version.env && \
    echo "MAXMEM=$MAXMEM" >> version.env && \
    rm -rf "/tmp/*" "/var/lib/apt/lists/*" "/var/tmp/*" "/var/cache/apk/*"

COPY startup.sh .
RUN chmod +x startup.sh

EXPOSE 5076
VOLUME ["/config", "/downloads"]

HEALTHCHECK --interval=1m --timeout=5s --start-period=2m \
    CMD /usr/bin/curl -fsSL "http://localhost:5076/system/about" && echo "OK" || exit 1

# Tini - init for containers. See https://github.com/krallin/tini
ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "/app/nzbhydra2/bin/startup.sh" ]

# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-nzbhydra2"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="nzbhydra2"
LABEL org.label-schema.schema-version="$NZBHYDRA2_VER"
